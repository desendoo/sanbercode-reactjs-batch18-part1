// ===== Soal 1 =====
console.log("===== Soal 1 =====");
// Rumus Luas Lingkaran menggunakan Jari-jari (L = phi x r^2)
const luasLingkaran = () => {
    let jariJari = 7;
    const hasil = (22/7) *  Math.pow(jariJari, 2);
    console.log("Luas lingkaran dengan jari-jari 7 adalah " + hasil); // 154
}
// Rumus Keliling Lingkaran menggunakan Jari-jari (K = phi x 2 x r)
const kelilingLingkaran = () => {
    let jariJari = 3;
    const hasil = (22/7) * 2 * jariJari;
    console.log("Keliling lingkaran dengan jari-jari 3 adalah " + hasil); // 18.857142857142858
}
luasLingkaran();
kelilingLingkaran();
console.log();
// END of Soal 1

// ===== Soal 2 =====
console.log("===== Soal 2 =====");
function funcLiteral(kata) {
    return `${kata} `;
}

let kalimat = "";
let kataKata = ["saya", "adalah", "seorang", "frontend", "developer"];

let hasil = ``;
kataKata.forEach(function(data) {
    hasil += funcLiteral(data);
});
kalimat = hasil;
console.log(kalimat);
console.log();
// END of Soal 2

// ===== Soal 3 =====
/*
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }  
}
*/
console.log("===== Soal 3 =====");
const newFunction = function literal(firstName, lastName) {
    return {
        firstName,
        lastName,
        fullName(){
            console.log(firstName + " " + lastName);
        }
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName();
console.log();
// END of Soal 3

// ===== Soal 4 =====
console.log("===== Soal 4 =====");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);
console.log();
// END of Soal 4

// ===== Soal 5 =====
console.log("===== Soal 5 =====");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];
//Driver Code
console.log(combined);
// END of Soal 5